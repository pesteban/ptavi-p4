#!/usr/bin/python3
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys

# Constantes. Dirección IP del servidor y contenido a enviar
REG_MESS = "REGISTER sip:# SIP/2.0\r\nExpires: @\r\n\r\n"
try:
    if len(sys.argv) == 6 and sys.argv[3].lower() == "register":
        IP = sys.argv[1]
        PORT = int(sys.argv[2])
        USERNAME = sys.argv[4]
        EXPIRES = int(sys.argv[5])
    else:
        sys.exit("usage error: python3 client.py ip port register username expires")
except ValueError:
        sys.exit("usage error: python3 client.py ip port register username expires")

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((IP, PORT))
    mess = REG_MESS.replace("@", str(EXPIRES)).replace("#", USERNAME)
    print("Enviando:", mess)
    my_socket.send(bytes(mess, 'utf-8'))
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")
