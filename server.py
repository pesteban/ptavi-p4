#!/usr/bin/python3
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
from datetime import datetime, date, time, timedelta

try:
    PORT = int(sys.argv[1])
    FORMAT = "%Y-%m-%d %H:%M:%S"
except:
    sys.exit("usage error: python3 server.py port")


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    dicc = {}

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        self.json2register()
        mess = ""
        for line in self.rfile:
            mess += line.decode('utf-8') + "\r\n"
        if "REGISTER" in mess:
            ip = self.client_address[0]
            port = int(self.client_address[1])
            for p in mess.split("\r\n"):
                if "sip:" in p:
                    user = p.split(":")[-1].split()[0]
                if "Expires" in p:
                    exp_time = int(p.split()[-1])
                    expires = datetime.now() + timedelta(seconds=exp_time)
            if exp_time == 0:
                try:
                    del self.dicc[user]
                    print(user, "deleted")
                    self.wfile.write(bytes("SIP/2.0 200 OK\r\n", "utf-8"))
                except:
                    pass
            else:
                print("REGISTER received from", user)
                self.dicc[user] = {"Address": ip + ":" + str(port),
                                   "Expires": expires.strftime(FORMAT)}
                self.wfile.write(bytes("SIP/2.0 200 OK\r\n", "utf-8"))
        self.register2json()
with open("registered.json", "w") as f_json:
            json.dump(self.dicc, f_json, indent=2)
    def register2json(self):
        with open("registered.json", "w") as f_json:
            json.dump(self.dicc, f_json, indent=2)

    def json2register(self):
        try:
            with open("registered.json", "r") as f_json:
                json.load(self.dicc, f_json)
        except:
            pass

if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
